# Teoría de Circuitos II
Repositorio de la matería Teoría de Circuitos II de la Carrera de Ingeniería Electrónica de la Facultad Regional Córdoba de la Universidad Tecnológica Nacional.


## Notebooks

En esta [carpeta](notebooks) encontraran notebooks con ejemplos para resolver ejercicios en python.
